import React from "react";

function Landing() {
  return(
  <div className=" flex  bg-gray-50  mobile:flex-col-reverse ">
    
    <div className="w-1/2  my-auto mobile:my-4 mobile:text-center tablet:align-bottom tablet:m-auto mobile:w-full tablet:w-full ">
           
        <h1 className="  font-bold w-2/3 m-auto mobile:mt-5  text-4xl mobile:text-2xl tablet:text-3xl tablet:mt-5 mb-3 ">
            Your Top-Notch <br className="mobile:invisible tablet:invisible"></br> Hosting Services is Here.
        </h1>
        
        <p className="font-light  w-2/3 my-3 m-auto tablet:text-sm mobile:w-full mobile:px-10   mobile:text-center">
               We assure you a hosting experience better than anything that you’ve ever had so why compromise for less when you could have more with us?
        </p>
               <div className="w-2/3 mx-auto ">
                <button className=" bg-blue-600 py-2 tablet:text-xs font-black text-white mobile:mr-3  rounded-3xl px-8 border-2 mb-2 mt-8 tablet:mt-4 mobile:mt-4">Host With Us now</button>
                <button className="  border-black py-2 tablet:text-xs  font-black  rounded-3xl px-11 border-2  mb-2  mobile:mt-4">Domain search</button>
               
               </div>
         
         </div>

        <div className="laptop:w-1/2 mx-10 mobile:m-auto mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
            <img className="m-auto mobile:p-4" src="https://ik.imagekit.io/softylus/landingPNG_x238yM7RV_N0W.png"></img>
      </div>
  </div>

  
  );
}

export default Landing;
