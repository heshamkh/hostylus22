import React from "react";

function Header() {
  return(
  // <div className="w-full h-20 mb-10 align-middle bg-white shadow">
    
  //   <div className=" sm:w-full w-1/5 h-full align-middle inline-block">
  //     <img className="m-auto h-full w-1/2" src="https://ik.imagekit.io/softylus/logo_hostylus_91QTyXifp.svg"></img>
  //   </div>

  //   <div className="w-4/5 h-full align-middle inline-block text-right">
  //     <ul className="inline-block ">
  //       <li className="inline-block text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-2 px-3  py-7 hover:border-blue-800 "><a className="py-7 " href="#">Home</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2  py-7 hover:border-blue-800 "><a className=" py-7" href="#">Web Hosting</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2  py-7  hover:border-blue-800 "><a className=" py-7" href="#">Servers</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800 "><a  className="hover:font-bold py-7" href="#">Domains</a></li>
  //       <li className="inline text-sm hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800  "><a className=" py-7" href="#">More</a></li>
  //     </ul>

  //     <button className="px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md ml-1 text-white rounded-3xl font-medium text-sm "> client area</button>
  //   </div>
  // </div>
 <div className="lg:px-20 px-6  bg-white shadow flex flex-wrap items-center lg:py-0 mobile:py-0 tablet:py-0 py-3">
      <div className="flex-1 flex justify-between items-center">
       <a href="#">
         <img className="my-auto h-full ml-20 pl-3  tablet:ml-11 tablet:pl-0 mobile:ml-2" src="https://ik.imagekit.io/softylus/logo_hostylus_91QTyXifp.svg"></img>

         </a>
      </div>

    <div className="visible desktop:w-4/5 tablet:w-1/2 mobile:w-1/2 h-full align-middle inline-block text-right">
       <ul className="inline-block mobile:invisible tablet:invisible ">
         <li className="inline-block text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-2 px-3  py-7 hover:border-blue-800 "><a className="py-7 " href="#">Home</a></li>
         <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2  py-7 hover:border-blue-800 "><a className=" py-7" href="#">Web Hosting</a></li>
         <li className="inline text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2  py-7  hover:border-blue-800 "><a className=" py-7" href="#">Servers</a></li>
         <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800 "><a  className="hover:font-bold py-7" href="#">Domains</a></li>
         <li className="inline text-sm hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800  "><a className=" py-7" href="#">More</a></li>
       </ul>

       <button className=" mobile:invisible tablet:invisible  px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md ml-1 text-white rounded-3xl font-medium text-sm "> client area</button>
     </div>

 <label htmlFor="menu-toggle" className="pointer-cursor  mr-5  laptop:hidden desktop:hidden wide-desktop:hidden block "><svg className="fill-current text-gray-900" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><title>menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg></label>
  <input className="hidden" type="checkbox" id="menu-toggle" />
<div className="hidden lg:flex lg:items-center mobile:w-full tablet:w-full" id="menu">
    <nav className="mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
      <ul className="sm:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
        <li><a className="lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400" href="#">Home</a></li>
        <li><a className="lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400" href="#">Web Hosting</a></li>
        <li><a className="lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400" href="#">Domain</a></li>
        <li><a className="lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400" href="#">Servers</a></li>
        <li><a className="lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400" href="#">More</a></li> 
      </ul>
       <button className="px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md ml-1 text-white rounded-3xl font-medium text-sm tablet:mr-0 mobile:mr-0"> client area</button>

    </nav>
    <a href="#" className="sm:ml-4 flex items-center justify-start lg:mb-0 mb-4 pointer-cursor">
      <img className="rounded-full w-10 h-10 border-2 border-transparent hover:border-indigo-400" src="https://pbs.twimg.com/profile_images/1128143121475342337/e8tkhRaz_normal.jpg" alt="Andy Leverenz" />
    </a>

   </div>
<div>
  
  
 </div>

  </div>
  );
}

export default Header;
