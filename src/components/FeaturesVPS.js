import React from "react";
import Feature from "../components/featureBlock";

function FeaturesVPS() {
    return (
     <section className="features max-w-6xl mx-auto my-16 mobile:px-3">
                <h2 className="text-4xl text-black font-bold text-center">FEATURES</h2>
                <div className="features grid grid-cols-3 gap-3 flex justify-items-center mt-6 mobile:grid-cols-1 py-8">
                    
                    <Feature image={"https://ik.imagekit.io/softylus/network_1zSrjHDaLP.svg"}
                             header={"Network"}
                             desc={"Let your servers communicate through a private network and setup complex network topologies. Ideal for running a kubernetes cluster or a database server that should not be reachable publicly."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/voluems_MnXrjdc0PiD3.svg"}
                             header={"Volumes"}
                             desc={"Volumes offer highly available and reliable SSD storage for your cloud servers. You can expand each Volume to up to 10 TB at any time and you can connect them to your Hetzner cloud servers."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/per_Rme4NpBTl.svg"}
                             header={"Performance"}
                             desc={"AMD EPYC 2nd Gen and Intel® Xeon® Gold processors together with speedy NVMe SSDs mean you’ll profit from high performance hardware. redundant 10 Gbit network connection."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/Backups_6GOeEiavx-.svg"}
                             header={"Backups"}
                             desc={"Backups are copies of your server that we make automatically to keep your data safe. You can store up to 7 of them."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/FloatingIPS_YwHTq91Px.svg"}
                             header={"Floating IPS"}
                             desc={"Assign floating IPs to fit your needs. Use them on a redundant server or for a highly available server cluster."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/images_wKSDQM3Qq0Yo.svg"}
                             header={"Images"}
                             desc={"Ubuntu, Debian, Fedora, and more – you have a wide variety of operating systems to choose from, and of course, we provide the most-current release."}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/traficks_SRjF4iX14.svg"}
                             header={"Traffics"}
                             desc={"With 20 TB of included traffic, you’ll have lots of bandwidth for your projects, regardless of which Hostylus Cloud package you choose. "}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/locations_PTDt1PNRm.svg"}
                             header={"Location"}
                             desc={"We have our cloud servers hosted with cloud instances in  data centers in Nuremberg, Falkenstein (Germany) and Helsinki (Finland). "}
                    />

                    <Feature image={"https://ik.imagekit.io/softylus/ddso_protection_SUQTE5dIj.svg"}
                             header={"DDOS Protections"}
                             desc={"Hostylus Online will safeguard your Hostylus cloud servers using the latest hardware appliances and sophisticated perimeter security technologies."}
                    />

                </div>
            </section>
   
       )
    }

    export default FeaturesVPS;

