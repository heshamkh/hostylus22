import React from "react";

 function cards(){
    return(
        <section className="text-center mobile:hidden tablet:hidden">
            <div className="flex flex-row w-8/12  mt-5 mb-10 justify-center mx-auto ">
                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl  duration-500">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className=" mt-4">
                        <h3 className="text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className="text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</p>
                    </div>
                </div>

                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl duration-500">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className=" mt-4">
                        <h3 className="text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className=" text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</p>
                    </div>
                </div>

                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl duration-500">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className="mt-4">
                        <h3 className=" text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className=" text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literaturem</p>
                    </div>
                </div>
            </div>

            <div className="flex flex-row w-8/12  mb-12 pb-32 justify-center mx-auto ">
                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl duration-500">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className=" mt-4">
                        <h3 className="text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className="text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</p>
                    </div>
                </div>

                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl duration-500">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className=" mt-4">
                        <h3 className="text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className=" text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature</p>
                    </div>
                </div>

                <div className="flex flex-col w-1/3 mx-5 py-10 px-4  bg-transparent hover:bg-white hover:shadow-lg border-t-8 border-transparent hover:border-yellow-400 rounded-xl duration-500 ">
                    <div className="w-1/3">
                        <img src="https://ik.imagekit.io/softylus/make_them_happy_5JoBj5uzX.svg"></img>
                    </div>
                    <div className="mt-4">
                        <h3 className=" text-xl text-gray-900 font-bold leading-8">Worpress development</h3>
                    </div>
                    <div className=" text-gray-400 text-sm pb-6 pt-2 font-semibold">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literaturem</p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default cards;