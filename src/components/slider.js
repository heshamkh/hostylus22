import React from "react";
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
// import "../styles/slider-animations.css";
const content = [
    {
        class:"slide bg-blue-200",
        title:"Vulputate Mollis Ultricies",
        description:<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick-mark_Ab4SXd2bR.png')", listStylePosition: "inside"}} className="list-disc"><li>Aenean eu leo quam. Pellentesque ornare</li><li className="py-2"> lacinia quam venenatis vestibulum.</li><li>nean eu leo quam. Pellentesque ornare</li></ul>,
        button:"Host with us now",
        image:"https://hostylus.com/wp-content/uploads/2020/10/Hostylus_dark_logo.png"
    },
    {
        class:"slide bg-green-600",
        title:"Vulputate Mollis Ultricies",
        description:"Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.",
        button:"Host with us now",
        image:"https://hostylus.com/wp-content/uploads/2020/11/data-center.svg"

    },
    {
        class:"slide bg-pink-300",
        title:"Vulputate Mollis Ultricies",
        description:"Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.",
        button:"Host with us now",
        image:"https://hostylus.com/wp-content/uploads/2020/11/gem.svg"

    },
]
export default function MySlider () {
return(
    <section  className="myslider h-screen max-h-screen bg-gradient-to-tl from-dark-blue to-mid-blue">
        <Slider className="slider px-16 pt-28 pb-40" style={{height: '100% !important'}}
                autoplay={3000}
                previousButton={<svg className="w-8 h-8 text-white" fill="none"
                                     stroke="currentColor"
                                     viewBox="0 0 24 24"
                                     xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 19l-7-7 7-7"></path>
                </svg>}
                nextButton={<svg className="w-8 h-8 text-white" fill="none"
                                 stroke="currentColor"
                                 viewBox="0 0 24 24"
                                 xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7"></path>
                </svg>}
        >
            {content.map((item, index) => (
                <div
                    key={index}
                    className="slide h-full"
                >
                    <div className="grid grid-cols-2 px-32 h-full">

                        <div className="px-2 pt-32">
                            <h1 className="font-lexend-mega pb-4 text-white text-4xl">{item.title}</h1>
                            <p className="py-1 text-white text-lg font-lexend-deca">{item.description}</p>
                            <button className="uppercase font-lexend-deca rounded mt-2 p-3 bg-yellow-400">{item.button}</button>
                        </div>
                        <div className="h-full">
                            <img className="w-3/4  ml-16" src={item.image}/>
                        </div>

                    </div>
                </div>
            ))}
        </Slider>
    </section>
);
}