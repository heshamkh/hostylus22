import React from "react";
import PropTypes from "prop-types";
function PlanLI(props){
    return(
        <li className="text-sm text-gray-600 my-3 ">{props.LI}</li>
    )
}
export default PlanLI;
PlanLI.propTypes = {
    LI: PropTypes.string.isRequired, // must be a node and defined
};