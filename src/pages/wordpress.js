import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
import PlanCard from "../components/plan_card";
import LI from "../components/plan-li";
import Feature from "../components/featureBlock";
import FeatureList from "../components/featureLists";
import Accordion from "../components/accordion"
function wordpressHosting() {
    return (
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing smallHeader="More than content management system"
                     header="wordpress hosting"
                     desc="Boost your online reach without worrying about your website’s hosting plans. Our wide range of options will help you achieve your intended goals."
                     btnURL="/"
                     image="https://ik.imagekit.io/softylus/worpress_3AA2FdsQ5zgQ.png"

            />
            <section className=" max-w-3xl mx-auto my-16 mobile:pb-3">
                <h2 className="text-4xl text-black font-bold text-center">Upgraded plans today for a better tomorrow</h2>
                <h4 className="text-gray-400 text-center text-xl px-16 my-3">Unleash the technology of your mind, and elevate your business sales with our wordpress hosting plans</h4>
                <div className="grid grid-cols-7 mt-8">
                    <h3 className="col-span-3 text-right text-gray-400 font-bold text-xl">Bill Monthly</h3>
                    <label className="px-6"><input id="s2" type="checkbox" className="switch"/></label>
                    <h3 className="col-span-3 text-left text-gray-400 font-bold text-xl">Bill Annualy</h3>
                </div>
                <div className="grid grid-cols-3 gap-2 flex justify-items-center mt-6 mobile:grid-cols-1 py-8">
                    <PlanCard savings={17} header={"startup"} desc={"A basic plan to fit your startup needs"} price={9.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                              </ul>}/>
                    <PlanCard savings={11} header={"growing"} desc={"A basic plan to fit your startup needs"} price={6.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                              </ul>}/>
                    <PlanCard savings={11} header={"cruising"} desc={"A basic plan to fit your startup needs"} price={9.99} btnLink={"/"}
                              myItems={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside"}} className="list-disc">
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                                  <LI LI={"100% SSD cloud hosting"}/>
                              </ul>}/>
                </div>
            </section>
            <section className="features max-w-6xl mx-auto my-16 mobile:px-3">
                <h2 className="text-4xl text-black font-bold text-center">your easiest wordpress solutions</h2>
                <div className="features grid grid-cols-3 gap-3 flex justify-items-center mt-6 mobile:grid-cols-1 py-8">
                    <Feature image={"https://ik.imagekit.io/softylus/w_fZm3LlJ2cr.png"}
                             header={"WordPress Simplified"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/secure_yRMTVsAXi-3J.png"}
                             header={"Secure Against Attacks"}
                             desc={"Hardens your site by default, further enhanced with the Toolkit’s No security expertisenecessary."}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/stage_V7pftE60eTAO4.png"}
                             header={"Stage and Test"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/automate_KypZFGdS_lRO.png"}
                             header={"Run and Automate"}
                             desc={"Singularly or mass-execute updates to the WP core, themes or plugins. Monitor and run all your WordPress sites from one dashboard."}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/complexity_RQiAbQCsh.png"}
                             header={"Cut Out Complexity"}
                             desc={"One-click installer to initialize and configure WordPress from start to finish. One dashboard to mass-manage multiple WordPress instances."}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/simple_t8z5nRdEh.png"}
                             header={"Simple, but not Amateur"}
                             desc={"Get full control with WP-CLI, maintenance mode, debug management, search engine index management and more."}
                    />

                </div>
            </section>
            <section className="screens my-16 mobile:pb-3">
                <div className="grid grid-cols-2 my-32 mobile:justify-items-center mobile:grid-cols-1">
                    <div className="pl-32 mobile:px-0.5">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Build</h3>
                        <FeatureList header="easy installation"
                                     desc="The one-click installer of WordPress Toolkit does everything from start to finish – downloads WP, creates a DB with a DB user, creates an admin account in WordPress and initializes WordPress so that it’s fully ready for use."
                        />
                        <FeatureList header="Staging Environment"
                                     desc="With WordPress Toolkit you can clone your site and set up a staging environment for your experiments, new features and new ideas. You can sync to production when you’re ready."
                        />
                        <FeatureList header="Theme and Plugin Management"
                                     desc="Quickly find and install a plugin or theme on a WordPress instance or several instances at once. Activate and deactivate plugins. Bulk remove plugins and themes that are not needed. Install plugin and theme sets on existing sites."
                        />
                    </div>
                    <div style={{background:'url("https://ik.imagekit.io/softylus/right_screen_Pvjrjqghg.png")',backgroundSize:'contain',backgroundPosition:'center right',backgroundRepeat:'no-repeat'}}></div>
                    {/*<img src="https://ik.imagekit.io/softylus/right_screen_Pvjrjqghg.png"/>*/}
                </div>


                <div className="grid grid-cols-2 my-32 mobile:justify-items-center mobile:grid-cols-1">
                    {/*<img src="https://ik.imagekit.io/softylus/image__2__Y7qhucs8m.png"/>*/}
                    <div style={{backgroundImage:'url("https://ik.imagekit.io/softylus/left_screen_WyzGiutcR5YJY.png")',backgroundSize:'contain',backgroundPosition:'center left',backgroundRepeat:'no-repeat'}}></div>
                    <div className="pr-16 mobile:px-0.5">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Secure</h3>
                        <FeatureList header="Click Hardening"
                                     desc="Scan all WordPress sites with Plesk to identify and protect your core installations, no manual work needed. Simply check the items you wish to harden, click “Secure”, and you’re done."
                        />
                        <FeatureList header="No Security Expertise Needed"
                                     desc="WordPress Toolkit security scanner goes beyond the basics and implements the latest security recommendations and best practices from WP Codex and WP security experts."
                        />
                        <FeatureList header="Smart Updates"
                                     desc="Smart Updates analyzes your WordPress updates, identifies changes and decides whether to perform them, without breaking your production site. Stay updated to prevent security risks."
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 my-32 mobile:justify-items-center mobile:grid-cols-1">
                    <div className="pl-32 mobile:px-0.5">
                        <h3 className="font-bold text-3xl px-16 mobile:text-center">Run</h3>
                        <FeatureList header="Backup and Restore Points"
                                     desc="If something goes wrong, restore points and backups allow you to restore your site back to its previous state."
                        />
                        <FeatureList header="Debug Management"
                                     desc="Manage all important debug options in WordPress or manage debug options on a per-instance basis from a single interface."
                        />
                        <FeatureList header="Site Indexing"
                                     desc="Enable or disable search engines from indexing your site on a per-instance basis."
                        />
                        <FeatureList header="Maintenance Mode in WordPress Toolkit"
                                     desc="Activate WordPress maintenance mode when updating WordPress, plugins, or themes with a single click."
                        />
                        <FeatureList header="Command Line Interface"
                                     desc="Easily access WordPress Command Line Interface for all your WordPress instances. Import a database, create a new user, update themes and plugins using WP-CLI. Access a full range of cloning options with Cloning CLI."
                        />
                    </div>
                    <div style={{backgroundImage:'url("https://ik.imagekit.io/softylus/right_screen_Pvjrjqghg.png")',backgroundSize:'contain',backgroundPosition:'top right',backgroundRepeat:'no-repeat'}}></div>
                    {/*<img className="" src="https://ik.imagekit.io/softylus/right_screen_Pvjrjqghg.png"/>*/}
                </div>
            </section>
            <section className="Epic py-16">
                <div className="grid justify-items-center">
                    <h3 className="mobile:text-2xl text-center text-white text-4xl font-bold">What Makes Us Your Pick?</h3>
                    <p className="mobile:px-5 mobile:text-lg px-64 py-4 text-center text-white text-xl">We’re capable of giving you the full support you’ll need with your WordPress website from scratch with the help of our partner company that skillfully covers this part and more.</p>
                    <img className="pt-8 mobile:px-2" src="https://ik.imagekit.io/softylus/Epic_kCIxZOJEk.png"/>
                </div>
            </section>
            <section className="acc py-16 grid justify-items-center">
                <div className=" px-8 max-w-2xl">
                    <h3 className="text-center text-black mb-16 text-4xl font-bold">Entert level Let’s encrypt protection</h3>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>

                </div>
            </section>

        </Layout>
    )
}
export default wordpressHosting;
